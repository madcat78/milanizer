defmodule MilanizerWebTest do
  use ExUnit.Case
  doctest MilanizerWeb, import: true

  describe "test word substitution" do
    test "substituteWord" do
      assert Milanizer.substituteWord({"lavoro", "job"}, "se il tuo lavoro ti piace", 0) == "se il tuo job ti piace"
    end

    test "substituteText single" do
      assert Milanizer.substituteText([{"lavoro", "job"},{"chiamata", "call"}], "se il tuo lavoro ti piace", 0) == "se il tuo job ti piace"
    end

    test "substituteText double" do
      assert Milanizer.substituteText([{"lavoro", "job"},{"chiamata", "call"}], "se il tuo lavoro non ti piace così com'è possiamo fare una chiamata", 0) == "se il tuo job non ti piace così com'è possiamo fare una call"
    end

    test "substituteText with dictionary" do
      assert Milanizer.substituteText(Milanizer.dictionary, "Trovare il obbiettivo sul tuo lavoro nel tuo contesto in base alle tue esigenze", 0) == "Trovare il focus sul tuo job nel tuo context in base alle tue needs"
    end
  end
end
