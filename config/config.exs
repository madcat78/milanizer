# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :milanizer,
  ecto_repos: [Milanizer.Repo]

# Configures the endpoint
config :milanizer, MilanizerWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Qy2BqnO6QpqWQUPtHykOxrWT7cviMTwA+Ds0cs7OiGaugWeC1PkJYgz9Byyb7J2/",
  render_errors: [view: MilanizerWeb.ErrorView, accepts: ~w(html json)],
  pubsub_server: Milanizer.PubSub

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
