defmodule Milanizer do
  @moduledoc """
  Milanizer keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  @random 30

  def substituteWord({pattern, replacement}, text, random \\ @random) do
    if :rand.uniform(100) > random do
      String.replace(text, pattern, replacement)
    else
      text
    end
  end

  def substituteText(dict, text, random \\ @random) do
    Enum.reduce(dict, text, &substituteWord(&1, &2, random))
  end

  def dictionary() do
    %{
      "a cosa aspiriamo" => "vision",
      "cena" => "aperitivino",
      "chiamata" => "call",
      "collega" => "team member",
      "colleghi" => "team",
      "concentrarsi" => "focussarsi",
      "conformità" => "compliance",
      "contesto" => "context",
      "contribuente" => "contributor",
      "cosa facciamo" => "mission",
      "curiosa" => "curious",
      "curiosità" => "curiosity",
      "curioso" => "curious",
      "dipendente" => "Giargiana",
      "esigenza" => "need",
      "esigenze" => "needs",
      "esperta" => "expert",
      "esperto" => "expert",
      "fammi sapere" => "upgrade",
      "ferie" => "fatturare",
      "finanza" => "finance",
      "flusso" => "stream",
      "formazione" => "my learning",
      "futuro" => "business plan",
      "i costi" => "lo spending",
      "il lavoro" => "la job",
      "il tuo lavoro" => "la tua job",
      "il vostro lavoro" => "le vostre job",
      "lavoro" => "job",
      "noccioline" => "peanuts",
      "obbiettivo" => "focus",
      "obiettivo" => "focus",
      "pagare" => "scashare",
      "partecipante" => "contributor",
      "il passaggio di consegne" => "l'handover",
      "pranzo" => "smart lunch",
      "previsione" => "forecast",
      "professionalità" => "skills",
      "ricompensa" => "revenue",
      "scadenza" => "due date",
      "snella" => "lean",
      "snello" => "lean",
      "soldi" => "cash",
      "stronzata" => "shit",
      "ufficio" => "office",
      "valore" => "value",
      "!!!" => "TAAAC",
    }
  end
end
