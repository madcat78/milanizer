defmodule MilanizerWeb.PageController do
  use MilanizerWeb, :controller

  @default_text "Trovare il corretto obiettivo sul tuo lavoro nel tuo contesto in base alle tue esigenze"
  @max_history 5


  def index(conn, _params) do
    state = getState(conn, :lastTranslations)
    render(conn, "index.html", lastTranslations: state)
  end

  def create(conn, %{"text" => ""}) do
    translate(conn, @default_text)
  end
  def create(conn, %{"text" => text}) do
    translate(conn, text)
  end
  def create(conn, %{"clearHistory" => "true"}) do
    clearState(conn, :lastTranslations)
    |> render("index.html", lastTranslations: nil)
  end

  defp translate(conn, text) do
    milanText = Milanizer.substituteText(Milanizer.dictionary, text)
    conn = putState(conn, :lastTranslations, {text, milanText})
    state = getState(conn, :lastTranslations)
    render(conn, "create.html", lastTranslations: state)
  end

  defp getState(conn, key) do
    get_session(conn, key)
  end

  defp putState(conn, key, {origText, milanText}) do
    case getState(conn, key) do
      nil ->
        put_session(conn, key, [{origText, milanText}])
      state ->
        state = limitStateLength(state)
        put_session(conn, key, [{origText, milanText} | state])
    end
  end

  defp clearState(conn, key) do
    put_session(conn, key, nil)
  end

  defp limitStateLength(state) when length(state) >= @max_history do
    Enum.slice(state, 0, @max_history - 1)
  end
  defp limitStateLength(state) do
    state
  end
end
