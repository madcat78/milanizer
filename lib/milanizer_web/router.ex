defmodule MilanizerWeb.Router do
  use MilanizerWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", MilanizerWeb do
    pipe_through :browser

    get "/", PageController, :index
    resources "/", PageController, only: [:create]
  end

  # Other scopes may use custom stacks.
  # scope "/api", MilanizerWeb do
  #   pipe_through :api
  # end
end
