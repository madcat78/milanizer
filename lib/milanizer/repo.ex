defmodule Milanizer.Repo do
  use Ecto.Repo,
    otp_app: :milanizer,
    adapter: Ecto.Adapters.Postgres
end
