defmodule Milanizer.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Start the Ecto repository
      #Milanizer.Repo,
      # Start the PubSub system
      {Phoenix.PubSub, name: Milanizer.PubSub},
      # Start the endpoint when the application starts
      MilanizerWeb.Endpoint
      # Starts a worker by calling: Milanizer.Worker.start_link(arg)
      # {Milanizer.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Milanizer.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    MilanizerWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
